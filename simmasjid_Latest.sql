-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Jan 2022 pada 03.46
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simmasjid`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `judul` varchar(128) NOT NULL,
  `penulis` varchar(64) NOT NULL DEFAULT 'Administrator',
  `konten` text NOT NULL,
  `foto_utama` text NOT NULL,
  `tag` varchar(128) NOT NULL,
  `slug` text NOT NULL,
  `tanggal` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id`, `judul`, `penulis`, `konten`, `foto_utama`, `tag`, `slug`, `tanggal`) VALUES
(1, 'EDIT Pasfoto Dengan Ganti Gambar', 'Administrator', '<h1 style=\"text-align:right\"><strong>Tester</strong></h1>\r\n\r\n<p><strong>oke ini ngetest lagi</strong></p>\r\n\r\n<p><strong>justify content</strong></p>\r\n\r\n<p><strong>right content</strong></p>\r\n\r\n<p><strong>image&nbsp;</strong></p>\r\n\r\n<p><img alt=\"ok\" src=\"http://localhost/simmasjid/assets/img/Profile.png\" style=\"height:400px; width:400px\" /></p>\r\n', '1630936611-2021-09-06-035651.jpg', 'a,b,c,d,eeeuler,oke', 'edit-pasfoto-dengan-ganti-gambar', '2021-09-01'),
(2, 'Pasfoto Tanpa Ganti Gambar Coba Preview', 'Administrator', '<p><em><strong>Ini konten berformat</strong></em></p>\r\n\r\n<p><strong>DOKUMEN PRA</strong><strong>PROPOSAL SKRIPSI</strong></p>\r\n\r\n<table cellspacing=\"0\" xss=removed>\r\n <tbody>\r\n  <tr>\r\n   <td xss=removed>\r\n   <p>Nama Mahasiswa</p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p><strong>:</strong></p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p>Muhammad Aliyya Ilmi</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td xss=removed>\r\n   <p>NIM</p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p><strong>:</strong></p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p>165150200111050</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td xss=removed>\r\n   <p>Jurusan</p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p><strong>:</strong></p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p>Teknik Informatika</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td xss=removed>\r\n   <p>Program Studi</p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p><strong>: </strong></p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p>Teknik Informatika</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td xss=removed>\r\n   <p>Keminatan</p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p><strong>:</strong></p>\r\n   </td>\r\n   <td xss=removed>\r\n   <p>Rekayasa Perangkat Lunak</p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p> </p>\r\n', '1630508457-2021-09-01-050057.jpg', 'test,baru,preview', 'pasfoto-tanpa-ganti-gambar-coba-preview', '2021-09-01'),
(4, 'Pasfoto Tanpa Ganti Gambar Coba Preview New', 'Administrator', '<p><strong>Curriculum Vitae</strong></p>\r\n\r\n<p><strong>Personal Data</strong></p>\r\n\r\n<p>Full Name                     : Juliana Sagita</p>\r\n\r\n<p>Place, Date of Birth      : Bandung, July 23 1994</p>\r\n\r\n<p>Address                         : Jl. Ujung Atas No. 128, Bandung</p>\r\n\r\n<p>Gender                          : Female</p>\r\n\r\n<p>Marital Status                : Single</p>\r\n\r\n<p>Phone                            : 08123456789</p>\r\n\r\n<p>Email                             : julianasagita@gmail.com</p>\r\n', '1630551257-2021-09-02-045417.jpg', 'cv, data diri, curriculum vitae, tips', 'pasfoto-tanpa-ganti-gambar-coba-preview-new', '2021-09-02'),
(6, 'Berita Terbaru: Kronologi Malaysia Heran Covid RI Melandai', 'Administrator', '<p><strong>Jakarta, CNBC Indonesia</strong> - <a href=\"https://www.cnbcindonesia.com/tag/malaysia\">Malaysia </a>dikabarkan heran dengan Covid-19 RI. Angka kasus yang terus melandai membuat Negeri Jiran terheran-heran.</p>\r\n\r\n<p>Lalu bagaimana kronologinya?</p>\r\n\r\n<p>Ini bermula dari pernyataan seorang politisi setempat, pemimpin Partai Aksi Demokratik (DAP) Lim Kit Siang. Kala itu, ia bertanya kepada Menteri Kesehatan Malaysia Khairy Jamaluddin.</p>\r\n\r\n<p xss=removed><img alt=\"image\" src=\"https://akcdn.detik.net.id/visual/2021/06/10/istana-raja-malaysia_169.jpeg?w=715&q=90\" xss=removed></p>\r\n\r\n<p>Covid-19 hariannya menjadi kurang dari Malaysia bahkan kurang dari setengah seperti kemarin 8.955 kasus menjadi 20.988 kasus Malaysia?\" kata Lim, dikutip dari <em>Malaymail</em>, Senin (6/9/2021).</p>\r\n\r\n<p>\"Ini bukan mencari-cari kesalahan tetapi mencari cara untuk meningkatkan penanganan kita terhadap pandemi Covid-19 sehingga memenangkan perang melawannya.\"</p>\r\n\r\n<p>Ia pun mengatakan, Malaysia saat ini menjadi salah satu negara dengan kinerja terburuk di dunia dalam hal respons Covid-19 tahun ini. Dari data Our World in Data yang diterbitkan 1 September 2021, kasus baru negara per satu juta orang sekarang adalah 572,43.</p>\r\n\r\n<p>Ini jauh jika dibandingkan tiga negara ASEAN lain, di mana Indonesia 37,40, Filipina 126,95 dan Myanmar 61,27.</p>\r\n\r\n<p>Malaysia juga menduduki puncak angka kematian Covid-19 di kawasan, dengan 8,48 per juta orang. Vietnam berada di urutan kedua dengan 8,19 sementara kematian harian per satu juta orang di Indonesia adalah 2,36.</p>\r\n\r\n<p>\"Pada laju infeksi dan kematian saat ini, kami akan menembus angka 1,8 juta untuk total kumulatif kasus Covid-19 hari ini,\" kata Lim.</p>\r\n\r\n<p>Saat ini Malaysia mencatat rata-rata 20 ribu kasus baru per hari. Merujuk data <em>Worldometers</em>, kemarin Malaysia masih mencatat 20.396 kasus baru dengan 336 kematian.</p>\r\n\r\n<p>Total kasus infeksi di Malaysia, dihitung sejak Covid-19 muncul di 2020, adalah 1.844.835. Total kematian berjumlah 18.219.</p>\r\n\r\n<p><strong>Hidup dengan Virus</strong></p>\r\n\r\n<p>Sementara itu, di tengah masih &#39;meledaknya&#39; kasus Covid-19, Kementerian Kesehatan (Kemenkes) Malaysia malah menyatakan pemerintah bersiap untuk transisi dari pandemi Covid-19 ke fase endemi. Di mana pembukaan kembali sektor usaha akan kembali dilakukan.</p>\r\n\r\n<p>Menkes Khairy Jamaluddin mengatakan Malaysia akan menyesuaikan diri dengan pola pikir baru yakni \"hidup dengan virus\" November nanti. Pemerintah berjanji akan berhati-hati.</p>\r\n\r\n<p>\"Kementerian Kesehatan berkomitmen untuk membuka diri secara bertanggung jawab dan aman. Begitu kita mencapai fase endemi, lebih banyak sektor usaha akan terbuka sepenuhnya,\" katanya, dikutip The Star.</p>\r\n\r\n<p> </p>\r\n', '1630977732-2021-09-07-032212.jpg', 'malaysia, malaysia heran covid-19 ri melandai, covid-19 ,covid malaysia dan indonesia', 'berita-terbaru:-kronologi-malaysia-heran-covid-ri-melandai', '2021-09-07'),
(7, 'Berita Terkini PON Papua-eSport Masuki Tahap Pra-PON sebanyak 891 Titik WiFi Disiapkan', 'Ilmi', '<p><strong>TEMPO.CO</strong>,&nbsp;<strong>Jakarta</strong>&nbsp;- Cabang&nbsp;<a href=\"https://www.tempo.co/tag/esports\" rel=\"noopener\" target=\"_blank\">eSport</a>, yang menjadi cabang olahraga ekshibisi di Pekan Olahraga Nasional (PON) XX Papua, memasuki tahap Pra PON setelah menyelesaikan tahap kualifikasi provinsi. Sekretaris Jenderal Pengurus Besar Esport Indonesia, Frengky Ong mengatakan, tahap kualifikasi provinsi telah berlangsung hingga 5 September. Tahap Pra PON akan dimulai 7-15 September.</p>\r\n\r\n<p>Ada empat nomor cabang esport yang akan dipertandingkan pada PON XX Papua. Keempat nomor tersebut adalah Free Fire, Mobile Legends, PES 2021, dan PUBG Mobile. Sementara satu nomor lagi yakni Lokapala hanya memainkan pertandingan persahabatan</p>\r\n\r\n<p>Pada tahap Pra PON sebanyak 34 provinsi akan memperebutkan 11 slot tim Free Fire, lima orang PES, lima tim Mobile Legends dan 15 tim PUBG Mobile. &quot;Untuk jadwal pertandingan ekshibisi esport di PON XX Papua sendiri sudah kita jadwalkan digelar selama lima hari mulai 22-26 September dengan venue Lapangan Hoki dan Kriket Doyobaru&quot; kata Frengky Ong pada Senin, 6 September 2021.</p>\r\n\r\n<p>Terpilihnya eSport sebagai cabang eksibisi&nbsp;<a href=\"https://www.tempo.co/tag/pon-papua\" rel=\"noopener\" target=\"_blank\">PON XX Papua</a>&nbsp;berdasarkan Instruksi Presiden No 4 Tahun 2021 tentang Dukungan Penyelenggaraan Pekan Olahraga Nasional. Tujuannya agar ekosistem esport Tanah Air bisa terus maju dan menelurkan bibit-bibit atlet esport yang mampu membawa nama Indonesia berkibar di kancah dunia.</p>\r\n', '1630978245-2021-09-07-033045.jpg', 'cv, data diri, curriculum vitae, tips', 'berita-terkini-pon-papua-esport-masuki-tahap-pra-pon-sebanyak-891-titik-wifi-disiapkan', '2021-09-07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detil_galeri`
--

CREATE TABLE `detil_galeri` (
  `id` int(4) NOT NULL,
  `id_galeri` int(4) NOT NULL,
  `judul` text NOT NULL,
  `foto` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `detil_galeri`
--

INSERT INTO `detil_galeri` (`id`, `id_galeri`, `judul`, `foto`, `keterangan`) VALUES
(3, 9, 'test 1', '1630852441-2021-09-050434010.jpg', 'o'),
(5, 9, 'ok tester update tambah', '1630897104-2021-09-060458240.jpg', 'o'),
(6, 9, 'hello', '1630935406-2021-09-060336460.jpg', 'ok');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri`
--

CREATE TABLE `galeri` (
  `id` int(11) NOT NULL,
  `judul` text NOT NULL,
  `foto` text NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `galeri`
--

INSERT INTO `galeri` (`id`, `judul`, `foto`, `keterangan`, `tanggal`) VALUES
(6, 'Foto ke2', '1630334925-2021-08-30-044845.jpg', 'Ngetest upload 2', '2021-08-30'),
(9, 'Pasfoto', '1630852441-2021-09-05-043401.jpg', 'okeoke', '2021-09-05'),
(10, 'Pasfoto Tanpa Ganti Gambar Coba Preview', '1630935488-2021-09-06-033808.jpg', 'Okeoke', '2021-09-06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_imam_tarawih`
--

CREATE TABLE `jadwal_imam_tarawih` (
  `id` int(4) NOT NULL,
  `sholat` varchar(7) NOT NULL,
  `nama_imam` varchar(64) NOT NULL,
  `nama_petugas_nida` varchar(64) NOT NULL DEFAULT '-',
  `waktu` text NOT NULL,
  `tahun_masehi` int(4) NOT NULL,
  `tahun_hijriah` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jadwal_imam_tarawih`
--

INSERT INTO `jadwal_imam_tarawih` (`id`, `sholat`, `nama_imam`, `nama_petugas_nida`, `waktu`, `tahun_masehi`, `tahun_hijriah`) VALUES
(1, 'Tarawih', 'H Abdullah Aji', 'M Urip Langgeng', '1-6 Ramadhan', 2021, 1441),
(5, 'Tasbih', 'H. Afandi', '-', '11-20 Ramadhan', 2021, 1441);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_konsumsi_ramadhan`
--

CREATE TABLE `jadwal_konsumsi_ramadhan` (
  `id` int(11) NOT NULL,
  `jenis_konsumsi` varchar(5) NOT NULL,
  `tanggal` int(2) NOT NULL,
  `tahun` int(4) NOT NULL,
  `tahunm` int(4) NOT NULL,
  `nama` varchar(48) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jadwal_konsumsi_ramadhan`
--

INSERT INTO `jadwal_konsumsi_ramadhan` (`id`, `jenis_konsumsi`, `tanggal`, `tahun`, `tahunm`, `nama`, `alamat`) VALUES
(2, 'Buka', 1, 1442, 2021, 'Muhammad Aliyya Ilmi', 'Dsn. Kebondalem - Kandangan'),
(3, 'Sahur', 2, 1442, 2021, 'MUHAMMAD ALIYYA ILMI', 'Dsn. Kebondalem - Kandangan'),
(5, 'Buka', 3, 1442, 2021, 'Muhammad Aliyya Ilmi', 'Dsn. Kebondalem - Kandangan'),
(6, 'Buka', 12, 1442, 2021, 'Muhammad Aliyya Ilmi', 'Dsn. Kebondalem - Kandangan'),
(7, 'Buka', 4, 1442, 2021, 'MUHAMMAD ALIYYA ILMI', 'Dsn. Kebondalem - Kandangan'),
(12, 'Sahur', 3, 1442, 2021, 'M. Ichwan', 'Dsn. Kebondalem - Kandangan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_pengajian_ramadhan`
--

CREATE TABLE `jadwal_pengajian_ramadhan` (
  `id` int(11) NOT NULL,
  `tanggal` int(2) NOT NULL,
  `pembicara` varchar(48) NOT NULL,
  `tema` text NOT NULL,
  `konsumsi` varchar(48) NOT NULL,
  `tahun_masehi` int(4) NOT NULL,
  `tahun_hijriah` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jadwal_pengajian_ramadhan`
--

INSERT INTO `jadwal_pengajian_ramadhan` (`id`, `tanggal`, `pembicara`, `tema`, `konsumsi`, `tahun_masehi`, `tahun_hijriah`) VALUES
(1, 1, 'KH Mansur', 'Menyambut Ramadhan', 'Ilmi', 2021, 1442),
(2, 2, 'KH Mansur', 'Ikhlas Bersedekah', 'Ilmi', 2021, 1442),
(3, 14, 'MUH A ILMI', 'Kesabaran', 'Ilmi', 2021, 1442);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_petugas_sholat_jumat`
--

CREATE TABLE `jadwal_petugas_sholat_jumat` (
  `id` int(11) NOT NULL,
  `jumat` varchar(10) NOT NULL,
  `bilal` varchar(36) NOT NULL,
  `khotib` varchar(36) NOT NULL,
  `jenis_khotib` varchar(10) NOT NULL DEFAULT 'utama'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jadwal_petugas_sholat_jumat`
--

INSERT INTO `jadwal_petugas_sholat_jumat` (`id`, `jumat`, `bilal`, `khotib`, `jenis_khotib`) VALUES
(7, 'Pon', 'M Rofiq', 'Ustadz Syakur', 'utama'),
(8, '-', 'Akhi Ilmi', 'H Farouq Ubaidillah', 'cadangan'),
(9, '-', 'Edit Bilal', 'khotib Cadangan Edited', 'cadangan'),
(10, 'Legi', 'Oke', 'khotib cadangan', 'utama');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kas`
--

CREATE TABLE `kas` (
  `id` int(11) NOT NULL,
  `mutasi` varchar(6) NOT NULL,
  `keterangan` text NOT NULL,
  `nominal` int(11) NOT NULL,
  `tanggal` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kas`
--

INSERT INTO `kas` (`id`, `mutasi`, `keterangan`, `nominal`, `tanggal`) VALUES
(1, 'Masuk', 'Infaq Edit data', 200000, '2021-08-27'),
(2, 'Masuk', 'Hibah', 150000, '2021-08-27'),
(5, 'Masuk', 'Infaq', 100000, '2021-08-30'),
(6, 'Masuk', 'Tambah masuk', 150000, '2021-07-31'),
(7, 'Keluar', 'Biaya konsumsi pengajian', 125000, '2021-08-31'),
(8, 'Keluar', 'Akomodasi tester', 75000, '2021-09-02'),
(9, 'Masuk', 'Infaq Edit data', 200000, '2021-09-06'),
(10, 'Masuk', 'Hibah', 150000, '2021-09-06'),
(11, 'Masuk', 'Infaq', 100000, '2021-09-06'),
(12, 'Masuk', 'Tambah masuk', 150000, '2021-09-06'),
(13, 'Keluar', 'Biaya konsumsi pengajian', 125000, '2021-09-06'),
(14, 'Keluar', 'Akomodasi tester', 75000, '2021-09-06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas_ramadhan`
--

CREATE TABLE `petugas_ramadhan` (
  `id` int(4) NOT NULL,
  `nama` varchar(48) NOT NULL,
  `waktu` text NOT NULL,
  `tugas` text NOT NULL,
  `tahun_hijriah` int(11) NOT NULL,
  `tahun_masehi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `petugas_ramadhan`
--

INSERT INTO `petugas_ramadhan` (`id`, `nama`, `waktu`, `tugas`, `tahun_hijriah`, `tahun_masehi`) VALUES
(3, 'Bpk. Huddin', '1 - 6 Ramadhan', 'Petugas Imsak', 1442, 2021),
(6, 'Karyo', 'Selama Bulan Ramadhan', 'Penjemput Konsumsi', 1442, 2021),
(7, 'M. Urip', 'Isya', 'Muadzin', 1441, 2021);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sarpras_pengurus`
--

CREATE TABLE `sarpras_pengurus` (
  `id` int(4) NOT NULL,
  `nama` text NOT NULL,
  `foto` text NOT NULL,
  `tanggal` date NOT NULL,
  `jenis` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sarpras_pengurus`
--

INSERT INTO `sarpras_pengurus` (`id`, `nama`, `foto`, `tanggal`, `jenis`) VALUES
(2, 'Tester', '1631980121-2021-09-18-054841.jpg', '2021-09-18', 'sarpras'),
(4, 'Kamar kecil', '1631982775-2021-09-18-063255.jpg', '2021-09-18', 'sarpras'),
(5, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '1632113283-2021-09-20-064803.jpg', '2021-09-20', 'pengurus'),
(6, 'Logo', '1632113408-2021-09-20-065008.jpg', '2021-09-20', 'sarpras'),
(7, 'Tester', '1632113788-2021-09-20-065628.jpg', '2021-09-20', 'sarpras');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(3) NOT NULL,
  `username` varchar(24) NOT NULL,
  `password` text NOT NULL,
  `nama` varchar(40) NOT NULL,
  `jabatan` varchar(15) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `jabatan`, `no_hp`, `alamat`, `created_at`) VALUES
(1, 'admin', '123456789', 'Administrator Edit', 'Admin', '000789456123', 'Dusun Kebondalem', '2021-08-26'),
(2, 'bendahara', 'bernad123', 'Bendahara Edit Sekretaris', 'Bendahara', '085784114468', 'Jombang', '2021-08-27'),
(5, 'sekretaris', '123456789', 'Sekretaris EDIT', 'Sekretaris', '0987654321', 'Jombang', '2021-09-18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `video`
--

CREATE TABLE `video` (
  `id` int(4) NOT NULL,
  `thumbnail` text NOT NULL,
  `judul` text NOT NULL,
  `link` text NOT NULL,
  `embed` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `video`
--

INSERT INTO `video` (`id`, `thumbnail`, `judul`, `link`, `embed`) VALUES
(1, '1631715365-2021-09-15-041605.jpg', 'CARA LIHAT FOTO PROFIL INSTAGRAM SECARA FULL ||BISA DOWNLOAD FOTO & STORY INSTAGRAM', 'https://www.youtube.com/watch?v=-vZOtMSxpXQ', 'https://www.youtube.com/embed?v=-vZOtMSxpXQ'),
(2, '1631766433-2021-09-16-062713.jpg', 'WAH MERINDING GUE DENGER NYA‼️ - SUSI PUDJIASTUTI - Deddy Corbuzier Podcast', 'https://www.youtube.com/watch?v=ZXFg3qI3cTU', 'https://www.youtube.com/embed?v=ZXFg3qI3cTU'),
(5, '1632275436-2021-09-22-035036.jpg', 'Kaesang', 'https://www.youtube.com/watch?v=11lZinWMG3o&list=RDCMUCYk4LJI0Pr6RBDWowMm-KUw&start_radio=1&ab_channel=DeddyCorbuzier', 'https://www.youtube.com/embed/11lZinWMG3o'),
(6, '1632276400-2021-09-22-040640.jpg', 'Tester', 'https://www.youtube.com/watch?v=11lZinWMG3o&list=RDCMUCYk4LJI0Pr6RBDWowMm-KUw&start_radio=1&ab_channel=DeddyCorbuzier', 'https://www.youtube.com/embed/11lZinWMG3o'),
(7, '1632292851-2021-09-22-084051.jpg', 'DPR', 'https://www.youtube.com/watch?v=EcGHzJe9lYc', 'https://www.youtube.com/embed/EcGHzJe9lYc'),
(8, '1632293107-2021-09-22-084507.jpg', 'Takbiran', 'https://www.youtube.com/watch?v=XFI2BB3AUIQ', 'https://www.youtube.com/embed/XFI2BB3AUIQ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `visi_misi`
--

CREATE TABLE `visi_misi` (
  `id` int(1) NOT NULL,
  `visi` text NOT NULL,
  `misi` text NOT NULL,
  `tgl_update` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `visi_misi`
--

INSERT INTO `visi_misi` (`id`, `visi`, `misi`, `tgl_update`) VALUES
(1, 'Test Edit Visi Misi visi masjid ', '<p>Misi Masjid</p>\r\n\r\n<ol>\r\n <li>Misi 1</li>\r\n <li>Misi 2</li>\r\n</ol>\r\n', '2021-09-20');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `detil_galeri`
--
ALTER TABLE `detil_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jadwal_imam_tarawih`
--
ALTER TABLE `jadwal_imam_tarawih`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jadwal_konsumsi_ramadhan`
--
ALTER TABLE `jadwal_konsumsi_ramadhan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jadwal_pengajian_ramadhan`
--
ALTER TABLE `jadwal_pengajian_ramadhan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jadwal_petugas_sholat_jumat`
--
ALTER TABLE `jadwal_petugas_sholat_jumat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kas`
--
ALTER TABLE `kas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `petugas_ramadhan`
--
ALTER TABLE `petugas_ramadhan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sarpras_pengurus`
--
ALTER TABLE `sarpras_pengurus`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `visi_misi`
--
ALTER TABLE `visi_misi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `detil_galeri`
--
ALTER TABLE `detil_galeri`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `jadwal_imam_tarawih`
--
ALTER TABLE `jadwal_imam_tarawih`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `jadwal_konsumsi_ramadhan`
--
ALTER TABLE `jadwal_konsumsi_ramadhan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `jadwal_pengajian_ramadhan`
--
ALTER TABLE `jadwal_pengajian_ramadhan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `jadwal_petugas_sholat_jumat`
--
ALTER TABLE `jadwal_petugas_sholat_jumat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `kas`
--
ALTER TABLE `kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `petugas_ramadhan`
--
ALTER TABLE `petugas_ramadhan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `sarpras_pengurus`
--
ALTER TABLE `sarpras_pengurus`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `video`
--
ALTER TABLE `video`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `visi_misi`
--
ALTER TABLE `visi_misi`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
